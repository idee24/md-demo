package momatechsoftwares.md_demo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import momatechsoftwares.md_demo.R;
import momatechsoftwares.md_demo.models.Landscape;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageListViewHolder>{
    List<Landscape> mdata;
    private LayoutInflater inflater;
    private Context context;

    public ImageListAdapter(Context context, List<Landscape> data){
        inflater = LayoutInflater.from(context);
        this.mdata = data;
        this.context = context;
    }

    @Override
    public ImageListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ImageListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageListViewHolder holder, int position) {
        Landscape current = mdata.get(position);
        holder.setData(context, current, position);
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    static class ImageListViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView image;
        ImageView deleteRow;
        ImageView addRow;
        int position;
        Landscape current;

        public ImageListViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.titleTextView);
            image = itemView.findViewById(R.id.image);
            deleteRow = itemView.findViewById(R.id.img_row_delete);
            addRow = itemView.findViewById(R.id.img_row_add);
        }

        public void setData(Context context, Landscape current, int position){
            this.title.setText(current.getTitle());
            this.image.setImageResource(current.getImageID());
            this.position = position;
            this.current = current;
        }
    }
}
