package momatechsoftwares.md_demo.activities;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import momatechsoftwares.md_demo.R;
import momatechsoftwares.md_demo.adapters.ImageListAdapter;
import momatechsoftwares.md_demo.models.Landscape;

public class MainActivity extends AppCompatActivity {

    View parentGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        refreshImageRecyclerView();

        parentGroup = findViewById(android.R.id.content);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.media_route:
                showSnack("Media-Route");
                break;
            case R.id.discard:
                showSnack("Discard");
                break;
            case R.id.search:
                showSnack("Search");
                break;
            case R.id.settings:
                showSnack("Settings");
                break;
            case R.id.edit:
                showSnack("Edit");
                break;
            case R.id.exit:
                showSnack("Exit");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSnack(String msg) {
        Snackbar.make(parentGroup, msg, Snackbar.LENGTH_SHORT).show();
    }

    private void refreshImageRecyclerView(){

        RecyclerView recyclerView = findViewById(R.id.imageRecyclerView);
        ImageListAdapter adapter = new ImageListAdapter(this, Landscape.getData());
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }
}
